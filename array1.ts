const names: string[] = [];
names.push("Dylan");
names.push("Foo");
// names.push(3);
console.log(names[0]);
console.log(names[1]);
console.log(names.length);

for (let i = 0; i < names.length; i++) {
    console.log(names[i]);   
}

for (const name in names) {
    console.log(name);
}

names.forEach(function(name) {
    console.log(name);
})