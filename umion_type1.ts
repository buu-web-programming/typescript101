function printStatusCode(code: string | number){
    if (typeof code === "string") {
        console.log('My code is ${code.toUpperCase()} ${typeof code}');
    } else {
        console.log('My code is ${code} ${typeof code}');
    }
}

printStatusCode(404);
printStatusCode("abc");